# frozen_string_literal: true

require "spec_helper"

# rubocop:disable Style/BlockLength
RSpec.describe GreatCircle::Service::ReadObjectsFromFile do
  describe "call" do
    let(:file_name) { "my_file.json" }
    subject(:read) { described_class.new(file_name).call }

    context "when file doesn't exist" do
      before { allow(File).to receive(:file?).with(file_name).and_return(false) }

      it "Raises an error" do
        expect { read }.to raise_error(GreatCircle::Error::FileError, "File not found.")
      end
    end

    context "when file exists" do
      before { allow(File).to receive(:file?).with(file_name).and_return(true) }
      before { allow(File).to receive(:readlines).with(file_name).and_return(lines) }

      context "when JSON is valid" do
        let(:lines) do
          [
            '{"latitude": "53.1489345", "user_id": 31, "name": "Alan Behan", "longitude": "-6.8422408"}',
            '{"latitude": "53.1229599", "user_id": 6, "name": "Theresa Enright", "longitude": "-6.2705202"}'
          ]
        end
        let(:hash1) do
          {
            "latitude" => "53.1489345",
            "user_id" => 31,
            "name" => "Alan Behan",
            "longitude" => "-6.8422408"
          }
        end

        let(:hash2) do
          {
            "latitude" => "53.1229599",
            "user_id" => 6,
            "name" => "Theresa Enright",
            "longitude" => "-6.2705202"
          }
        end

        it { is_expected.to eq([hash1, hash2]) }
      end

      context "when JSON is invalid" do
        let(:lines) do
          [
            '{"latitude": "53.1489345", "user_id": 31, "name": "Alan Behan", "longitude": "-6.8422408"}',
            '{"latitude": "53.1229599, "user_id": 6, "name": "Theresa Enright", "longitude": "-6.2705202"}'
          ]
        end
        it "Raises an error indicating affected line." do
          expect { read }.to raise_error GreatCircle::Error::FileError, "Invalid JSON at line 2."
        end
      end
    end
  end
end
# rubocop:enable Style/BlockLength
