# frozen_string_literal: true

require "spec_helper"

RSpec.describe GreatCircle::Service::DistanceBetweenPlaces do
  describe "call" do
    subject(:call) { described_class.new(point_a, point_b).call }

    shared_context "assert_distance" do
      it { is_expected.to eq(distance) }
    end

    context "From Spire to my house" do
      let(:point_a) { GreatCircle::Model::Place.new("53.349917953649694", "-6.259954394148728") }
      let(:point_b) { GreatCircle::Model::Place.new("53.34084692247839", "-6.424581486208369") }
      let(:distance) { 11 }

      include_context "assert_distance"
    end

    context "From Spire to Cliffs" do
      let(:point_a) { GreatCircle::Model::Place.new("53.349917953649694", "-6.259954394148728") }
      let(:point_b) { GreatCircle::Model::Place.new("52.97210449541408",  "-9.431097001073601") }
      let(:distance) { 216 }

      include_context "assert_distance"
    end

    context "From Spire to Jericoacoara Sunset Dune" do
      let(:point_a) { GreatCircle::Model::Place.new("53.349917953649694",  "-6.259954394148728") }
      let(:point_b) { GreatCircle::Model::Place.new("-2.7992547574575637", "-40.51918371075609") }
      let(:distance) { 7_008 }

      include_context "assert_distance"
    end
  end
end
