# frozen_string_literal: true

require "spec_helper"

RSpec.describe GreatCircle::Service::DegreesToRadians do
  describe "call" do
    # 6 decimal places checking
    subject(:call) { described_class.new(value).call.truncate(6).to_s("F") }

    shared_context "assert_value" do
      it { is_expected.to eq(expected_value) }
    end

    context do
      let(:value)          { BigDecimal("53.34084692247839") }
      let(:expected_value) { "0.930973" }
      include_context "assert_value"
    end

    context do
      let(:value)          { BigDecimal("0") }
      let(:expected_value) { "0.0" }
      include_context "assert_value"
    end
  end
end
