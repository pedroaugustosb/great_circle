# frozen_string_literal: true

require "spec_helper"

# rubocop:disable Style/BlockLength
RSpec.describe GreatCircle::Service::FilterInCircle do
  describe "call" do
    # Spire
    let(:place) { GreatCircle::Model::Place.new("53.349917953649694", "-6.259954394148728") }

    let(:p1) { { "name" => "Lucan", "latitude" => "53.34084692247839", "longitude" => "-6.424581486208369" } }
    let(:p2) { { "name" => "Liffey Valley", "latitude" => "53.35288024218858", "longitude" => "-6.3913889015512435" } }
    let(:p3) { { "name" => "Ha'penny Bridge", "latitude" => "53.34656800584698", "longitude" => "-6.263101786208237" } }
    let(:p4) { { "name" => "Intercom", "latitude" => "53.33947863353452", "longitude" => "-6.257640374566519" } }

    let(:elements) do
      [p1, p2, p3, p4]
    end

    let(:sort) { false }

    subject(:call) do
      described_class.new(
        reference_place: place,
        distance: distance,
        elements: elements,
        sort: sort
      ).call
    end

    context "5 Km" do
      let(:distance) { 5 }
      it { is_expected.to match_array([p3, p4]) }
    end

    context "10 Km" do
      let(:distance) { 10 }
      it { is_expected.to match_array([p2, p3, p4]) }
    end

    context "15 Km" do
      let(:distance) { 15 }
      it { is_expected.to match_array([p1, p2, p3, p4]) }

      context "sorting" do
        let(:sort) { "name" }
        it { is_expected.to eq([p3, p4, p2, p1]) }
      end
    end
  end
end
# rubocop:enable Style/BlockLength
