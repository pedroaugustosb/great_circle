# frozen_string_literal: true

require "spec_helper"

RSpec.describe GreatCircle::Model::Place do
  describe "coordinates_in_radians" do
    subject(:coordinates) { place.coordinates_in_radians }

    shared_context "assert_coordinates" do
      it { is_expected.to eq([lat, lon]) }
    end

    context "Spire" do
      let(:place) { GreatCircle::Model::Place.new("53.349917953649694", "-6.259954394148728") }
      let(:lat) { BigDecimal("0.9311321230204342042902") }
      let(:lon) { BigDecimal("-0.1092568620273959944024") }

      include_context "assert_coordinates"
    end

    context "Cliffs" do
      let(:place) { GreatCircle::Model::Place.new("52.97210449541408", "-9.431097001073601") }
      let(:lat) { BigDecimal("0.924538031389810562464") }
      let(:lon) { BigDecimal("-0.1646037652888378803333") }

      include_context "assert_coordinates"
    end
  end
end
