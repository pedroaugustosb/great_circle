# frozen_string_literal: true

RSpec.describe GreatCircle do
  it "has a version number" do
    expect(GreatCircle::VERSION).not_to be nil
  end
end
