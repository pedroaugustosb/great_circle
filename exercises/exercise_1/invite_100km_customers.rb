# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path("../../lib", __dir__)
require "great_circle"

# Load customers list from file
customers = GreatCircle::Service::ReadObjectsFromFile.new("exercises/exercise_1/customers.txt").call

# Filtering only customers from 100Km radius
dublin_office = GreatCircle::Model::Place.new("53.339428", "-6.257664")
invited_customers = GreatCircle::Service::FilterInCircle.new(
  reference_place: dublin_office,
  distance: 100,
  elements: customers,
  sort: "user_id"
).call

# Generates output file
File.open("exercises/exercise_1/output.txt", "w") do |file|
  invited_customers.each { |customer| file.puts customer.to_json }
end
