2. Proudest Achievement

What's your proudest achievement? It can be a personal project or something you've worked on professionally. Just a short paragraph is fine, but I'd love to know why you're proud of it, what impact it had (If any) and any insights you took from it.

Answer:

I believe my recent proudest achievent was a large project that I accepted to do entirely by myself in November of 2016.
I usually don't work on side projects but at that time it was a need as I was organizing my wedding.
This project was a big CRM for a warehouse rental company. Some of the implemented features were:
- Inclusion of clients data;
- National Bank payments system integration;
- Financial reports;
- Time based e-mails sending;
- Graphic Dashboard;
- Automated contracts renewal;
- Automated deploy and hosting in Amazon Web Services;
- Cost management of services used in cloud;
At the beginning, it appeared to be a never-ending project as it had a huge backlog.
After little more than a year working in my free time, I was able to finish all requested features and deploy them to production.
I used my strongest skills at that time as stack: Rails, AWS, Postgres and Javascript.
During this project I could not only evolve as a developer as but also I learned how to interact with the client and how to deal with them about the deadlines and deliveries.
After this time I could finish the project in a friendly way with the client very satisfied.
I helped him with the search of someone to maintain the project and helped this new person to get familiarized with the project.
It was a tough time working many hours per day but it worthed it and made me a better professional.
