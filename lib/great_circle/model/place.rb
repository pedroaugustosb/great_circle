# frozen_string_literal: true

require_relative "../service/degrees_to_radians"

module GreatCircle
  module Model
    ##
    # Represents a place on Earth surface
    #
    class Place
      attr_reader :latitude, :longitude

      # Initializes with Coordinates
      def initialize(latitude, longitude)
        @latitude  = latitude.to_d
        @longitude = longitude.to_d
      end

      def coordinates_in_radians
        [latitude_in_radians, longitude_in_radians]
      end

      private

      def latitude_in_radians
        ::GreatCircle::Service::DegreesToRadians.new(@latitude).call
      end

      def longitude_in_radians
        ::GreatCircle::Service::DegreesToRadians.new(@longitude).call
      end
    end
  end
end
