# frozen_string_literal: true

module GreatCircle
  module Service
    ##
    # Given an array of hashes, it filters its elements
    # by a custom distance from a specific place using elements'
    # latitude and longitude keys
    # Optional sort hash key parameter can be given
    #
    class FilterInCircle
      def initialize(reference_place:, distance:, elements:, sort: false)
        @reference_place = reference_place
        @distance = distance
        @elements = elements
        @sort     = sort
      end

      def call
        output = filter
        output.sort_by! { |element| element[sort] } if sort
        output
      end

      private

      def filter
        elements.select do |element|
          place = GreatCircle::Model::Place.new(element["latitude"], element["longitude"])
          current_distance = ::GreatCircle::Service::DistanceBetweenPlaces.new(reference_place, place).call

          current_distance <= distance
        end
      end

      attr_reader :reference_place, :distance, :elements, :sort
    end
  end
end
