# frozen_string_literal: true

require "bigdecimal/util"

module GreatCircle
  module Service
    ##
    # Converts coordinates from degrees to radians
    #
    class DegreesToRadians
      RADIANS_OF_A_DEGREE = "0.0174533".to_d

      def initialize(value)
        @value = value
      end

      def call
        RADIANS_OF_A_DEGREE * value
      end

      private

      attr_reader :value
    end
  end
end
