# frozen_string_literal: true

require "bigdecimal"

module GreatCircle
  module Service
    EARTH_RADIUS_IN_KM = BigDecimal("6_371")

    ##
    # Calculates distance between 2 places A and B
    #
    class DistanceBetweenPlaces
      def initialize(place_a, place_b)
        @latitude_a, @longitude_a = place_a.coordinates_in_radians
        @latitude_b, @longitude_b = place_b.coordinates_in_radians
      end

      def call
        (EARTH_RADIUS_IN_KM * central_angle).round
      end

      private

      def central_angle
        product1 = sin_latitude_a * sin_latitude_b
        product2 = cos_latitude_a * cos_latitude_b * cos_delta_longitude

        Math.acos(product1 + product2)
      end

      def sin_latitude_a
        Math.sin(latitude_a)
      end

      def sin_latitude_b
        Math.sin(latitude_b)
      end

      def cos_latitude_a
        Math.cos(latitude_a)
      end

      def cos_latitude_b
        Math.cos(latitude_b)
      end

      def cos_delta_longitude
        delta = (longitude_b - longitude_a).abs
        Math.cos(delta)
      end

      attr_reader :latitude_a, :latitude_b, :longitude_a, :longitude_b
    end
  end
end
