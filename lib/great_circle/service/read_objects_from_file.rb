# frozen_string_literal: true

require "json"

module GreatCircle
  module Service
    ##
    # Reads each line of file as JSON and converts them to hashes
    #
    class ReadObjectsFromFile
      def initialize(file_name)
        @file_name = file_name
      end

      def call
        validate_existance

        File.readlines(file_name).map.with_index do |line, index|
          JSON.parse(line)
        rescue JSON::ParserError
          line_number = index + 1
          raise GreatCircle::Error::FileError, "Invalid JSON at line #{line_number}."
        end
      end

      private

      def validate_existance
        raise GreatCircle::Error::FileError, "File not found." unless File.file?(file_name)
      end

      attr_reader :file_name
    end
  end
end
