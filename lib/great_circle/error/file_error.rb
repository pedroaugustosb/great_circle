# frozen_string_literal: true

module GreatCircle
  module Error
    ##
    # Error class related to file management
    #
    class FileError < StandardError
      DEFAULT_MESSAGE = "Error while reading file."

      def initialize(message = DEFAULT_MESSAGE)
        super message
      end
    end
  end
end
