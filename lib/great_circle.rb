# frozen_string_literal: true

require_relative "great_circle/version"

##
# This gem provides helper methods to calculate
# distance between places on Earth surface
#
module GreatCircle
  module Model
    autoload :Place, "great_circle/model/place"
  end

  module Service
    autoload :DistanceBetweenPlaces, "great_circle/service/distance_between_places"
    autoload :DegreesToRadians,      "great_circle/service/degrees_to_radians"
    autoload :ReadObjectsFromFile,   "great_circle/service/read_objects_from_file"
    autoload :FilterInCircle,        "great_circle/service/filter_in_circle"
  end

  module Error
    autoload :FileError, "great_circle/error/file_error"
  end
end
