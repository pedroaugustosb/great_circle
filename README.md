# GreatCircle

This solution is built as a Ruby Gem. I used ruby 2.6 to solve it.

## Installation

```
cd great_circle
gem install bundler
bundle install
```

### Run problem solution

The solutions are inside **exercise** folder. Questions 1 and 2. To run the first:

```
ruby exercises/exercise_1/invite_100km_customers.rb
```

The answer for question 2 is in **exercises/exercise_2/answer.txt**

### Run specs
```
bundle exec rspec
```

## Thank You

> pedro.augusto.sb@gmail.com
> +353 118 8863
